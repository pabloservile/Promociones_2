/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author VOIPASER
 */
public class Movimiento {

    private int fechaOrigen;
    private int fechaPresentado;
    private String nroL;
    private String leyenda;
    private int rubro;
    private double importeLocal;
    private double importeDolar;
    private String tipoConsumo;
    private String codop;
    private int nro4;
    private String grupoMovimiento;
    private String flag;

    public Movimiento(int fechaOrigen, int fechaPresentado, String nroL, String leyenda, int rubro, double importeLocal, double importeDolar, String tipoConsumo, String codop, int nro4, String grupoMovimiento, String flag) {
        this.fechaOrigen = fechaOrigen;
        this.fechaPresentado = fechaPresentado;
        this.nroL = nroL;
        this.leyenda = leyenda;
        this.rubro = rubro;
        this.importeLocal = importeLocal;
        this.importeDolar = importeDolar;
        this.tipoConsumo = tipoConsumo;
        this.codop = codop;
        this.nro4 = nro4;
        this.grupoMovimiento = grupoMovimiento;
        this.flag = flag;
    }

    public int getFechaOrigen() {
        return fechaOrigen;
    }

    public int getFechaPresentado() {
        return fechaPresentado;
    }

    public String getNroL() {
        return nroL;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public int getRubro() {
        return rubro;
    }

    public double getImporteLocal() {
        return importeLocal;
    }

    public double getImporteDolar() {
        return importeDolar;
    }

    public String getTipoConsumo() {
        return tipoConsumo;
    }

    public String getCodop() {
        return codop;
    }

    public int getNro4() {
        return nro4;
    }

    public String getGrupoMovimiento() {
        return grupoMovimiento;
    }

    public String getFlag() {
        return flag;
    }
    
    

}
