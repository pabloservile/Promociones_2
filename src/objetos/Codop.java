/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author VOIPASER
 */
public class Codop {

    private String nroCodop;
    private String moneda;
    private String grupoMov;
    private String porcentaje;
    private String producto;
    private double topeMl;
    private double topeUsd;
    private String fechaDesde;
    private String fechaHasta;
    private String diasAplica;
    private String leyenda;
    private String grupoAff;

    public Codop() {
    }

    public Codop(String nroCodop, String moneda, String grupoMov, String procentaje, String producto, double topeMl, double topeUsd, String fechaDesde, String fechaHasta, String diasAplica, String leyenda, String grupoAff) {
        this.nroCodop = nroCodop;
        this.moneda = moneda;
        this.grupoMov = grupoMov;
        this.porcentaje = procentaje;
        this.producto = producto;
        this.topeMl = topeMl;
        this.topeUsd = topeUsd;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.diasAplica = diasAplica;
        this.leyenda = leyenda;
        this.grupoAff = grupoAff;
    }

    public String getNroCodop() {
        return nroCodop;
    }

    public String getMoneda() {
        return moneda;
    }

    public String getGrupoMov() {
        return grupoMov;
    }

    public String getProcentaje() {
        return porcentaje;
    }

    public String getProducto() {
        return producto;
    }

    public double getTopeMl() {
        return topeMl;
    }

    public double getTopeUsd() {
        return topeUsd;
    }

    public String getFechaDesde() {
        return fechaDesde;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public String getDiasAplica() {
        return diasAplica;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public String getGrupoAff() {
        return grupoAff;
    }

    public void setNroCodop(String nroCodop) {
        this.nroCodop = nroCodop;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void setGrupoMov(String grupoMov) {
        this.grupoMov = grupoMov;
    }

    public void setProcentaje(String procentaje) {
        this.porcentaje = procentaje;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public void setTopeMl(double topeMl) {
        this.topeMl = topeMl;
    }

    public void setTopeUsd(double topeUsd) {
        this.topeUsd = topeUsd;
    }

    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setDiasAplica(String diasAplica) {
        this.diasAplica = diasAplica;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }

    public void setGrupoAff(String grupoAff) {
        this.grupoAff = grupoAff;
    }
    

}
