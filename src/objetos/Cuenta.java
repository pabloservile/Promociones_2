/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author VOIPASER
 */
public class Cuenta {

    private int banco;
    private long cuenta;
    private long tarjeta;
    private double monedaLocal;
    //Cambio mod liq x rubro(MCC)
    private int modLiq;
    private int grupoAff;
    private String producto;

    public Cuenta(int banco, long cuenta, long tarjeta, double monedaLocal, int modLiq, int grupoAff, String producto) {
        this.banco = banco;
        this.cuenta = cuenta;
        this.tarjeta = tarjeta;
        this.monedaLocal = monedaLocal;
        this.modLiq = modLiq;
        this.grupoAff = grupoAff;
        this.producto = producto;
    }

    public int getBanco() {
        return banco;
    }

    public long getCuenta() {
        return cuenta;
    }

    public long getTarjeta() {
        return tarjeta;
    }

    public double getMonedaLocal() {
        return monedaLocal;
    }

    public int getModLiq() {
        return modLiq;
    }

    public int getGrupoAff() {
        return grupoAff;
    }

    public String getProducto() {
        return producto;
    }
    
    
    
    
    
    
    

}
