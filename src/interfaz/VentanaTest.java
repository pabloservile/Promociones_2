/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import negocio.ExportarCodops;
import negocio.ImportarCodops;
import static negocio.ImportarCodops.listaCodop;
import objetos.Codop;

/**
 *
 * @author VOIPASER
 */
public class VentanaTest extends javax.swing.JFrame {

    private int fila = 0;
    private int strCodop;
    private int strRubro;
    private String strGrupoMov;
    private String strPorcentaje;
    private String strProducto;
    private double strTopeMl;
    private double strTopeUsd;
    private String strDesde;
    private String strHasta;
    private String strDiasAplica;
    private String strLeyenda;
    private int strGrupoAff;

    /**
     * Creates new form VentanaTest
     */
    public VentanaTest() throws Exception {
        initComponents();
        ImportarCodops impCod = new ImportarCodops();
        //impCod.importarExcel();
        impCod.importarTxt();

        for (Codop c : listaCodop) {
            jTable1.setValueAt(c.getNroCodop(), fila, 0);
            jTable1.setValueAt(c.getMoneda(), fila, 1);
            jTable1.setValueAt(c.getGrupoMov(), fila, 2);
            jTable1.setValueAt(c.getProcentaje(), fila, 3);
            jTable1.setValueAt(c.getProducto(), fila, 4);
            jTable1.setValueAt(c.getTopeMl(), fila, 5);
            jTable1.setValueAt(c.getTopeUsd(), fila, 6);
            jTable1.setValueAt(c.getFechaDesde(), fila, 7);
            jTable1.setValueAt(c.getFechaHasta(), fila, 8);
            jTable1.setValueAt(c.getDiasAplica(), fila, 9);
            jTable1.setValueAt(c.getLeyenda(), fila, 10);
            jTable1.setValueAt(c.getGrupoAff(), fila, 11);

            fila++;
        }

    }

    public boolean validarFormulario() {
        if (txtCodop.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Codop no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtCodop.requestFocus();
            return false;
        }

        // Valida que el codop sea un numero entero
        try {
            Integer.parseInt(txtCodop.getText());
        } catch (Exception NumberFormatException) {
            JOptionPane.showMessageDialog(this, "El Codop debe ser un numero entero!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtCodop.requestFocus();
            return false;
        }

        // Valida que la moneda sea un numero entero
        try {
            Integer.parseInt(txtMoneda.getText());
        } catch (Exception NumberFormatException) {
            JOptionPane.showMessageDialog(this, "La Moneda debe ser un numero entero!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtMoneda.requestFocus();
            return false;
        }

        

        // Valida que el descuento sea un numero entero
        try {
            Double.parseDouble(txtDescuento.getText());
        } catch (Exception NumberFormatException) {
            JOptionPane.showMessageDialog(this, "El Descuento debe ser un numero entero!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtDescuento.requestFocus();
            return false;
        }

      

        // Valida que el TOPE ML sea un numero que puede tener decimales
        try {
            Double.parseDouble(txtTopeML.getText());
        } catch (Exception NumberFormatException) {
            JOptionPane.showMessageDialog(this, "El TOPE ML debe ser un numero!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtTopeML.requestFocus();
            return false;
        }

        // Valida que el presupuesto sea un numero que puede tener decimales
        try {
            Double.parseDouble(txtTopeUsd.getText());
        } catch (Exception NumberFormatException) {
            JOptionPane.showMessageDialog(this, "El TOPE USD debe ser un numero!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtTopeUsd.requestFocus();
            return false;
        }

        // Valida que el producto no este vacio
        if (txtProducto.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Producto no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtProducto.requestFocus();
            return false;
        }

        // Valida que el Desde no este vacio
        if (txtDesde.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Desde no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtDesde.requestFocus();
            return false;
        }

        // Valida que el Hasta no este vacio
        if (txtHasta.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Hasta no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtHasta.requestFocus();
            return false;
        }

        // Valida que el Dias Aplica no este vacio
        if (txtDiasAplica.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Dias Aplica no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtDiasAplica.requestFocus();
            return false;
        }

        // Valida que el Moneda no este vacio
        if (txtMoneda.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "La Moneda no puede estar vacia!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtMoneda.requestFocus();
            return false;
        }

        // Valida que el GrupoAff no este vacio
        if (txtGrupoAff.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Grupo Mov no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtGrupoAff.requestFocus();
            return false;
        }

        // Valida que el Descuento no este vacio
        if (txtDescuento.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Descuento no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtDescuento.requestFocus();
            return false;
        }

        // Valida que el Producto no este vacio
        if (txtProducto.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Producto no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtProducto.requestFocus();
            return false;
        }

        // Valida que el Tope ML no este vacio
        if (txtTopeML.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El Tope ML no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtTopeML.requestFocus();
            return false;
        }

        // Valida que el Tope USD no este vacio
        if (txtTopeUsd.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "El TOPE USD no puede estar vacio!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtTopeUsd.requestFocus();
            return false;
        }

       

        // Valida que el Leyenda no este vacio
        if (txtLeyenda.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "La Leyenda no puede estar vacias!", "Guardar", JOptionPane.WARNING_MESSAGE);
            txtLeyenda.requestFocus();
            return false;
        }

        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtCodop = new javax.swing.JTextField();
        txtMoneda = new javax.swing.JTextField();
        txtGrupoAff = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtDescuento = new javax.swing.JTextField();
        txtProducto = new javax.swing.JTextField();
        txtTopeML = new javax.swing.JTextField();
        txtTopeUsd = new javax.swing.JTextField();
        txtDesde = new javax.swing.JTextField();
        txtHasta = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtDiasAplica = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtCuotas = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        txtLeyenda = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Ejecutar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Codop", "Rubro", "Grupo Mov", "Descuento", "Producto", "Tope ML", "Tope U$D", "Desde", "Hasta", "Dias Aplica", "Leyenda", "Aff. Group"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable1);

        txtCodop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodopActionPerformed(evt);
            }
        });

        txtMoneda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMonedaActionPerformed(evt);
            }
        });

        txtGrupoAff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGrupoAffActionPerformed(evt);
            }
        });

        jButton2.setText("Agregar Codop");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Codop");

        jLabel2.setText("Rubro");

        jLabel3.setText("Grupo Mov");

        jLabel4.setIcon(new javax.swing.ImageIcon("D:\\logo-visa.jpg")); // NOI18N

        jLabel5.setText("Descuento");

        jLabel6.setText("Producto");

        jLabel7.setText("Tope ML");

        jLabel8.setText("Tope USD");

        jLabel9.setText("Desde");

        jLabel10.setText("Hasta");

        txtDescuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescuentoActionPerformed(evt);
            }
        });

        txtTopeUsd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTopeUsdActionPerformed(evt);
            }
        });

        txtHasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHastaActionPerformed(evt);
            }
        });

        jLabel11.setText("Dias Aplica");

        jLabel12.setText("Leyenda");

        jLabel13.setText("Grupo Aff");

        txtCuotas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCuotasActionPerformed(evt);
            }
        });

        jButton3.setText("Limpiar Formulario");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Eliminar Codop");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jCheckBox1.setText("Sumariza");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 766, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton2)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel8)
                                                        .addComponent(jLabel11))
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(4, 4, 4)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel1)
                                                            .addComponent(jLabel5))))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addComponent(txtDiasAplica)
                                                    .addComponent(txtDescuento, javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtCodop, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                                    .addComponent(txtTopeUsd, javax.swing.GroupLayout.Alignment.LEADING))
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                            .addGap(36, 36, 36)
                                                            .addComponent(jLabel12))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                            .addGap(34, 34, 34)
                                                            .addComponent(jLabel6)))
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                        .addGap(47, 47, 47)
                                                        .addComponent(jLabel9)))))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtDesde, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                                            .addComponent(txtProducto)
                                            .addComponent(jButton3)
                                            .addComponent(txtLeyenda))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtGrupoAff, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabel7))
                                        .addGap(19, 19, 19)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtHasta, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtTopeML, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jCheckBox1))))
                                    .addComponent(jButton4)))
                            .addComponent(jButton1))
                        .addContainerGap(90, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGrupoAff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTopeML, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(txtTopeUsd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtHasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtDiasAplica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLeyenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtTopeUsdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTopeUsdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTopeUsdActionPerformed

    private void txtDescuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescuentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescuentoActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:

        boolean validacionOK = validarFormulario();

        if (validacionOK) {
            jTable1.setValueAt(txtCodop.getText(), fila, 0);
            jTable1.setValueAt(txtMoneda.getText(), fila, 1);
            jTable1.setValueAt(txtGrupoAff.getText(), fila, 2);
            jTable1.setValueAt(txtDescuento.getText(), fila, 3);
            jTable1.setValueAt(txtProducto.getText(), fila, 4);
            jTable1.setValueAt(txtTopeML.getText(), fila, 5);
            jTable1.setValueAt(txtTopeUsd.getText(), fila, 6);
            jTable1.setValueAt(txtDesde.getText(), fila, 7);
            jTable1.setValueAt(txtHasta.getText(), fila, 8);
            jTable1.setValueAt(txtDiasAplica.getText(), fila, 9);
            jTable1.setValueAt(txtCuotas.getText(), fila, 11);
            jTable1.setValueAt(txtLeyenda.getText(), fila, 10);
            fila++;

        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtGrupoAffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGrupoAffActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGrupoAffActionPerformed

    private void txtCodopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodopActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodopActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        ExportarCodops expCod = new ExportarCodops();
        try {
            listaCodop.clear();
            for (int filaJtable = 0; filaJtable < jTable1.getRowCount(); filaJtable++) {

                if (jTable1.getValueAt(filaJtable, 0) != null) {

                    
                    String a = (String) jTable1.getModel().getValueAt(filaJtable, 0);
                    String b = (String) jTable1.getModel().getValueAt(filaJtable, 1).toString();
                    String m = (String) jTable1.getModel().getValueAt(filaJtable, 2);
                    String d = (String) jTable1.getModel().getValueAt(filaJtable, 3);
                    String e = (String) jTable1.getModel().getValueAt(filaJtable, 4);
                    double f = Double.parseDouble(jTable1.getModel().getValueAt(filaJtable, 5).toString());
                    double g = Double.parseDouble(jTable1.getModel().getValueAt(filaJtable, 6).toString());
                    String h = (String) jTable1.getModel().getValueAt(filaJtable, 7);
                    String i = (String) jTable1.getModel().getValueAt(filaJtable, 8);
                    String j = (String) jTable1.getModel().getValueAt(filaJtable, 9);
                    String k = (String) jTable1.getModel().getValueAt(filaJtable, 10);
                    String l = (String) jTable1.getModel().getValueAt(filaJtable, 11);
                    Codop c = new Codop(a, b, m, d, e, f, g, h, i, j, k, l);
                    listaCodop.add(c);
                }

            }

            expCod.exportarCodops(listaCodop);
        } catch (IOException ex) {
            Logger.getLogger(VentanaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(VentanaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
        Ventana v = new Ventana();
        v.setSize(700, 700);
        v.pack();

        v.setResizable(true);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dim.getWidth() - v.getWidth()) / 2);
        int y = (int) ((dim.getHeight() - v.getHeight()) / 2);
        v.setLocation(x, y);
        v.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtCuotasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCuotasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCuotasActionPerformed

    private void txtHastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHastaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHastaActionPerformed

    private void txtMonedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMonedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMonedaActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        txtCodop.setText("");
        txtMoneda.setText("");
        txtGrupoAff.setText("");
        txtDescuento.setText("");
        txtProducto.setText("");
        txtTopeML.setText("");
        txtTopeUsd.setText("");
        txtDesde.setText("");
        txtHasta.setText("");
        txtDiasAplica.setText("");
        txtLeyenda.setText("");
        txtCuotas.setText("");
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:

        fila = fila - 1;
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new VentanaTest().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(VentanaTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtCodop;
    private javax.swing.JTextField txtCuotas;
    private javax.swing.JTextField txtDescuento;
    private javax.swing.JTextField txtDesde;
    private javax.swing.JTextField txtDiasAplica;
    private javax.swing.JTextField txtGrupoAff;
    private javax.swing.JTextField txtHasta;
    private javax.swing.JTextField txtLeyenda;
    private javax.swing.JTextField txtMoneda;
    private javax.swing.JTextField txtProducto;
    private javax.swing.JTextField txtTopeML;
    private javax.swing.JTextField txtTopeUsd;
    // End of variables declaration//GEN-END:variables
}
