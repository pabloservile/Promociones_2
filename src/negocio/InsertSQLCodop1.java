/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import static negocio.ControlarLANT.BCO;
import static negocio.ControlarLANT.CARTERA;

/**
 *
 * @author Pablo Servile
 */
public class InsertSQLCodop1 {

    public void insertSqlCodop() throws Exception {
        System.out.println("INSTER SQL CODOP");
        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();
        File archivoTxt = new File("L:/Liqui/"+BCO+"_"+CARTERA+"_"+System.getProperty("user.name")+"_Codop.csv");
        // Arma la sentencia de inserción y la ejecutaFile archivoTxt = new File(archivoTxtPago);

        if (!archivoTxt.exists()
                || !archivoTxt.canRead()) {
            throw new Exception("Archivo inaccesible");
        } else {

            //String laInsercion = "LOAD DATA INFILE \'D:\\Lant.csv\' INTO TABLE movimientos FIELDS TERMINATED BY \',\' ENCLOSED BY \'\"\'LINES TERMINATED BY \'\\n\';";
            String path = "L:/Liqui/"+BCO+"_"+CARTERA+"_"+System.getProperty("user.name")+"_"+"Codop.csv";
            String laInsercion = " LOAD DATA LOCAL INFILE '" + path
                    + "' INTO TABLE codops "
                    + " FIELDS TERMINATED BY \',\' ENCLOSED BY \'\"'"
                    + " LINES TERMINATED BY \'\\n\'";

            stmtInsercion = laConexion.createStatement();
            stmtInsercion.execute(laInsercion);

            laConexion.close();

        } // Si llego al final del archivo, termina la ejecuci�n

        stmtInsercion.close();

    }

}
