/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import static interfaz.Ventana.file2;
import java.sql.Connection;
import java.sql.Statement;
import javax.swing.JOptionPane;


/**
 *
 * @author VOIPASER
 */
public class Ejecutar extends Thread {

    public static boolean band = false;
    

    @Override
    public void run() {
        band = false;
        try {
            this.ejecutarQuerys();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verificar Ejecutar Query");
        }
        band = true;
        
    }

    public void ejecutarQuerys() throws Exception {
        ControlarLANT a = new ControlarLANT();
        System.out.println(file2);
        a.controlarTxt(file2);
        InsertSQLLant1 insSqlLant = new InsertSQLLant1();
        insSqlLant.insertSqlLant();
        insSqlLant = null;
        InsertSQLCodop1 insSQLCodop1 = new InsertSQLCodop1();
        insSQLCodop1.insertSqlCodop();
        insSQLCodop1 = null;
        //ConsultaLaColonia consLaColonia = new ConsultaLaColonia();
        //consLaColonia.queryLaColonia();
        //ConsultaLarach consLarach = new ConsultaLarach();
        //consLarach.queryLarach();
        EjecutarQuery e = new EjecutarQuery();
        e.queryLaColonia();
        e.queryLarach();
        e.queryKielsa();
        e.queryKielsaOtros();
        e.queryIndura();
        e.queryDiunsa();
        e.querySears();
        e.queryDescFicohsaUno();
        deleteTables();

       // JOptionPane.showMessageDialog(null, "Proceso Finalizado");

    }

    public static void deleteTables() throws Exception {

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();
        // Arma la sentencia de inserción y la ejecutaFile archivoTxt = new File(archivoTxtPago);

        //String laInsercion = "LOAD DATA INFILE \'D:\\Lant.csv\' INTO TABLE movimientos FIELDS TERMINATED BY \',\' ENCLOSED BY \'\"\'LINES TERMINATED BY \'\\n\';";
        String laInsercion = "DELETE FROM `consumos` WHERE 1";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        laInsercion = "DELETE FROM `codops` WHERE 1";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        laConexion.close();

        stmtInsercion.close();
        System.out.println("Delete Tablas Finalizado");
    }

}
