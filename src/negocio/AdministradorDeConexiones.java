/*
 * AdministradorDeConexiones.java
 *
 *
 */

package negocio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sebasti�n S. Sanga <SebastianSanga@gmail.com>
 */
public abstract class AdministradorDeConexiones {
    
    /**
     * Creates a new instance of AdministradorDeConexiones
     */
    public AdministradorDeConexiones() {
    }

    public static Connection getConnection() throws Exception 
    {

        // Establece el nombre del driver a utilizar
        String dbDriver = "org.gjt.mm.mysql.Driver";
        
        // Establece la conexion a utilizar contra la base de datos
        String dbConnString = "jdbc:mysql://localhost:3306/bdliqui?autoReconnect=true&useSSL=false";
        
        // Establece el usuario de la base de datos
        String dbUser = "root";
        
        // Establece la contrase�a de la base de datos
        String dbPassword = "";
        
        try {
            // Establece el driver de conexion
            Class.forName(dbDriver).newInstance();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AdministradorDeConexiones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Retorna la conexion
        return DriverManager.getConnection(dbConnString, dbUser, dbPassword);
    }    
    
}
