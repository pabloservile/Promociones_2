 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import objetos.Codop;

/**
 *
 * @author VOIPASER
 */
public class Comparar {

    private double descuentoLant;
    private double descuentoCalculado = 0.0;
    private String lineaLeida;
    private String s;

    private String strCuenta;
    private String strRubro;
    private String strGAff;
    private String strProducto;
    private String strTarjeta;
    private String strFechaOrigen;
    private String strLeyenda;
    private String strImporteMl;
    private String strImporteUsd;
    private String strCodop;
    private String strGrupoMov;
    private String productoCodop;
    private String gaffCodop;

    Pattern patronMl = Pattern.compile("^.* (\\d+,\\d{2})\\-.*0,00.*0,00.*");
    Pattern patronUsd = Pattern.compile("");
    Pattern patMovimiento = Pattern.compile("^(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)");

    public void comparar(File file, List<Codop> listaCodops) throws Exception {

        Map<String, Double> mapDescuentos = new HashMap<String, Double>();

        int renglon = 0;
        String s = null;
        File archivoTxt = file;
        System.out.println("Lista Codops: " + listaCodops);
        for (Codop c : listaCodops) {
            System.out.println("C.getCodop: " + c.getNroCodop());
            if (!archivoTxt.exists() || !archivoTxt.canRead()) {
                throw new Exception("Archivo inaccesible");
            } else {

                BufferedReader readerMejorado = new BufferedReader(new FileReader(archivoTxt));
                boolean eof = false;
                while (!eof) {

                    lineaLeida = readerMejorado.readLine();

                    if (lineaLeida != null) {
                        renglon++;
                        //             System.out.println("renglon: " + renglon);
                        if (renglon == 1) {
                            s = lineaLeida.substring(0, 10);
                            //                     System.out.println(" S se declara como==== " + lineaLeida.substring(0, 10));
                        }

                        if (lineaLeida.endsWith("BON ( ) I010")) {

                            if (s.equals(lineaLeida.substring(0, 10))) {
                                //                         System.out.println("Si linea Leida  = a s : " + s + " :: " + lineaLeida.substring(0, 10));
                                //System.out.println("Todo bien");
                            } else {

                                //                         System.out.println("S no es igual a linealeida.substring 0 10 : " + lineaLeida.substring(0, 10) + " :: " + s);
                                //                         System.out.println("S" + s);
                                s = lineaLeida.substring(0, 10);
                                mapDescuentos.put(s, 0.0);
                                descuentoLant = 0.0;
                                //System.out.println("Lista promo: " + listaDoublePromo);

                            }

                            Matcher matchMl = patronMl.matcher(lineaLeida);
                            //System.out.println("Codop: " + lineaLeida);
                            if (matchMl.find()) {
                                String[] setCodop = (c.getNroCodop() + "").split("\\.");

                                for (String stringCodop : setCodop) {
                                    //System.out.println("String Codop: " + stringCodop);
                                    //String nroC = c.getNroCodop() + " ";
                                    //                         System.out.println("Substring 11 15" + lineaLeida.substring(11, 15).replaceFirst("^0+", ""));
                                    //                         System.out.println("Substring 16 22" + lineaLeida.substring(16, 22));
                                    //                         System.out.println("c.getaff" + c.getGrupoAff());
                                    //                         System.out.println("c.producto" + c.getProducto());
                                    String[] setGruposAff = c.getGrupoAff().split("\\.");

                                    for (String qwe : setGruposAff) {
                                        if (lineaLeida.contains(stringCodop) && lineaLeida.substring(11, 15).replaceFirst("^0+", "").equals(qwe) && lineaLeida.substring(16, 22).equals(c.getProducto())) {

                                            //System.out.println("Entro a las 3 condiciones ok: " );
                                            descuentoLant = Double.parseDouble(matchMl.group(1).replace(",", ".")) * -1;
                                            //System.out.println("Descuento Lant: " + descuentoLant);
                                            //listaDoublePromo.add(descuentoLant);
                                            if (mapDescuentos.isEmpty()) {
                                                //listaDoublePromo.add(descuentoLant);
                                                mapDescuentos.put(s, descuentoLant);

                                            } else if (mapDescuentos.containsKey(s)) {

                                                //listaDoublePromo.add(descuentoLant);
                                                descuentoLant = mapDescuentos.get(s) + descuentoLant;
                                                //                                    System.out.println("Descuento Lant Else IF: " + descuentoLant);
                                                mapDescuentos.put(s, descuentoLant);

                                            } else {
                                                //listaDoublePromo.add(descuentoLant);

                                                descuentoLant = mapDescuentos.get(s) + descuentoLant;
                                                //                                    System.out.println("Descuento Lant Else: " + descuentoLant);
                                                mapDescuentos.put(s, descuentoLant);
                                            }

                                        }
                                    }
                                }
                            }

                        } else {
                            Matcher matchMov = patMovimiento.matcher(lineaLeida);

                            if (s.equals(lineaLeida.substring(0, 10))) {
                                //                        System.out.println("Si linea Leida  = a s : " + s + " :: " + lineaLeida.substring(0, 10) + " Mov");
                                //System.out.println("Todo bien");
                            } else {

                                //                        System.out.println("S no es igual a linealeida.substring 0 10 : " + lineaLeida.substring(0, 10) + " :: " + s);
                                //                        System.out.println("S" + s);
                                s = lineaLeida.substring(0, 10);
                                mapDescuentos.put(s, 0.0);
                                descuentoCalculado = 0.0;
                                //System.out.println("Lista promo: " + listaDoublePromo);

                            }

                            if (matchMov.find()) {
                                strCuenta = matchMov.group(1);
                                strRubro = matchMov.group(2);
                                strGAff = matchMov.group(3);
                                strProducto = matchMov.group(4);
                                strTarjeta = matchMov.group(5);
                                strFechaOrigen = matchMov.group(6);
                                strLeyenda = matchMov.group(7);
                                strImporteMl = matchMov.group(8);
                                strImporteUsd = matchMov.group(9);
                                strCodop = matchMov.group(10);
                                strGrupoMov = matchMov.group(11);
                                //System.out.println("strProducto: " + strProducto + " " + c.getProducto());
                                //System.out.println("C.getMoneda: " + c.getMoneda());
                                //System.out.println("StrMoneda: " + strRubro);

                                if (strRubro.equals(c.getMoneda()) || c.getMoneda().equals("*")) {
//Valida que el producto sea igual al del codop
                                    //System.out.println("Valido el rubro: " + strRubro);
                                    if (strProducto.equals(c.getProducto()) || c.getProducto().equals("*")) {

                                        String gaff = strGAff.replaceFirst("^0+", "");

///////////////////////////////////////////////////////////////
//Valida que el gaff sea igual al grupo de aff del codop         
////////////////////////////////////////////////////////////////
                                        //                            System.out.println(gaff);
                                        String codopGaff = c.getGrupoAff();
                                        String[] set = codopGaff.split("\\.");
// System.out.println("SET 0 : " + set[0]);
//String[] set = c.getGrupoAff().split(".");
//for (String qwe : set) {
//    System.out.println("AAAAAAAAAAAA " + qwe);
//}

                                        for (String qwe : set) {

                                            if (gaff.equals(qwe) || c.getGrupoAff().equals("*")) {
                                                //System.out.println("Gaff de ambos: " + c.getCuotas());
                                                //System.out.println("Leyendas: " + strLeyenda + " : " + c.getLeyenda());
//Valida que la leyenda del mov contenga la leyenda que indica el codop
                                                if (strLeyenda.contains(c.getLeyenda()) || c.getLeyenda().equals("*")) {
                                                    //System.out.println(c.getLeyenda());

//Valida que el movimiento no haya sido abonado con puntos                                            
                                                    if (!strCodop.equals("200501")) {
//Valida que el grupo movimiento sea igual "CON" por ej.                                                
                                                        if (strGrupoMov.equals(c.getGrupoMov()) || c.getGrupoMov().equals("*")) {
                                                            String dateLant = strFechaOrigen;
                                                            String dateCodopDesde = c.getFechaDesde();
                                                            String dateCodopHasta = c.getFechaHasta();

                                                            DateFormat formato = new SimpleDateFormat("ddMMyyyy");

                                                            Date dateLantD = formato.parse(dateLant);
                                                            Date dateCodopDesdeD = formato.parse(dateCodopDesde);
                                                            Date dateCodopHastaD = formato.parse(dateCodopHasta);

                                                            //System.out.println(strFechaOrigen);
                                                            //System.out.println(c.getFechaDesde());
                                                            //System.out.println(c.getFechaHasta());
// Valida que la fecha se encuentre en la                                                     
                                                            if ((dateLantD.compareTo(dateCodopDesdeD) > 0)
                                                                    && (dateLantD.compareTo(dateCodopHastaD) < 0)) {

                                                                //                                                    System.out.println("Entra el mov  " + strCuenta);
                                                                //                                                    System.out.println("Importe Movimiento : " + strImporteMl.replace(",", "."));
                                                                double dbImporteMl = Double.parseDouble(strImporteMl.replace(",", "."));

                                                                if (Double.parseDouble(strImporteMl.replace(",", ".")) != 0) {
                                                                    descuentoCalculado = Double.parseDouble(strImporteMl.replace(",", ".")) * Double.parseDouble(c.getProcentaje()) / 100;

                                                                    //                                                        System.out.println("Descuento Calculado: " + descuentoCalculado);
                                                                    if (mapDescuentos.isEmpty()) {
                                                                        //listaDoublePromo.add(descuentoLant);
                                                                        mapDescuentos.put(s, descuentoCalculado);

                                                                    } else if (mapDescuentos.containsKey(s)) {

                                                                        //listaDoublePromo.add(descuentoLant);
                                                                        descuentoCalculado = mapDescuentos.get(s) + descuentoCalculado;
                                                                        //                                                            System.out.println("Descuento Calculado IF: " + descuentoCalculado);
                                                                        mapDescuentos.put(s, descuentoCalculado);

                                                                    } else {
                                                                        //listaDoublePromo.add(descuentoLant);

                                                                        descuentoCalculado = mapDescuentos.get(s) + descuentoCalculado;
                                                                        //                                                            System.out.println("Descuento Calculado Else: " + descuentoCalculado);
                                                                        mapDescuentos.put(s, descuentoCalculado);
                                                                    }

                                                                } else {
                                                                    descuentoCalculado = Double.parseDouble(strImporteMl.replace(",", ".")) * Double.parseDouble(c.getProcentaje()) / 100;
                                                                    //                                                        System.out.println("Descuento Calculado: " + descuentoCalculado);

                                                                } // cierra el esle del descuento calculado

                                                            }//cierra el if de comparacion fechas

                                                        }//cierra la comparacion de los grupos de mov

                                                    }//cierra el if del codop != 200501

                                                }//cierra el if del codop de leyenda

                                            }//cierra el if del grupo de aff
                                        }//cierra ciclo for del grupoaff
                                    }//cierra el if del producto
                                }
                            }//cierra el if del movimiento

                        }//cierra el if de si es codop
                        //                System.out.println("-----------------------------------------------------------------");
                    } else {
                        eof = true;

                    }

                }//cierra while != eof

                readerMejorado.close();
                System.out.println("=============================================================");
                System.out.println("==========  CODOP " + c.getNroCodop() + "  ===================================");
                System.out.println("Sysout de Errores: ");
                for (Map.Entry entry : mapDescuentos.entrySet()) {
                    if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.06 <= Double.parseDouble(entry.getValue().toString()) || -0.06 >= Double.parseDouble(entry.getValue().toString()))) {
                        System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
                    }

                }
                System.out.println("Tamaño Mapa: " + mapDescuentos.size());
                //System.out.println("Tipo de cambio: " + tipoDeCambio);
                System.out.println("=============================================================");
                System.out.println("=============================================================");
                renglon = 0;

            } // cierra if si existe el archivo
        }//cierra for codops
    }// cierra comparar

    public static double redondear(double numero) {
        return Math.rint(numero * 100) / 100;

    }

    public void getCuentasPenalizadas() throws FileNotFoundException, IOException, ParseException {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyMMdd");
        String formatted = format1.format(cal.getTime());
        System.out.println(formatted);
// Output "2012-09-26"

        System.out.println(format1.parse(formatted));

        ArrayList<String> listaCuentasPenalizadas = new ArrayList<>();
        int bco = 730;
        File file = new File("L:\\Archivos Diarios Control\\");
        Pattern pattern = Pattern.compile("730_USUDES" + formatted + ".*");
        //BufferedReader readerMejorado = new BufferedReader(new FileReader(listFilesMatching(file,"730_USUDES" + format1.parse(formatted));
        boolean eof = false;

        while (!eof) {
        //    lineaLeida = readerMejorado.readLine();

            if (lineaLeida != null) {
            } else {
                eof = true;

            }

        }

        //return listaCuentasPenalizadas;
    }
    
    
     public static File[] listFilesMatching(File root, String regex) {
        if (!root.isDirectory()) {
            throw new IllegalArgumentException(root + " is no directory.");
        }
        final Pattern p = Pattern.compile(regex); // careful: could also throw an exception!
        return root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return p.matcher(file.getName()).matches();
            }
        });
    }

}
