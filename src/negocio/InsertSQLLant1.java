/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import static negocio.ControlarLANT.BCO;
import static negocio.ControlarLANT.CARTERA;

/**
 *
 * @author Pablo Servile
 */
public class InsertSQLLant1 {

    public void insertSqlLant() throws Exception {
        // Define la conexion
        System.out.println("INSERT SQL LANT");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();
        File archivoTxt = new File("L:/Liqui/"+BCO+"_"+CARTERA+"_"+System.getProperty("user.name")+"_Lant.csv");
        // Arma la sentencia de inserción y la ejecutaFile archivoTxt = new File(archivoTxtPago);
        System.out.println(" INSERT SQL LANT 2   "  + archivoTxt);
        if (!archivoTxt.exists() || !archivoTxt.canRead()) {
            throw new Exception("Archivo inaccesible");
        } else {
            System.out.println("ACA");

            String path = "L:/Liqui/"+BCO+"_"+CARTERA+"_"+System.getProperty("user.name")+"_"+"Lant.csv";
            String laInsercion = " LOAD DATA LOCAL INFILE '" + path
                    + "' INTO TABLE consumos "
                    + " FIELDS TERMINATED BY \',\' ENCLOSED BY \'\"'"
                    + " LINES TERMINATED BY \'\\n\'";

            stmtInsercion = laConexion.createStatement();
            stmtInsercion.execute(laInsercion);

            laConexion.close();

        }
        stmtInsercion.close();
    }

}
