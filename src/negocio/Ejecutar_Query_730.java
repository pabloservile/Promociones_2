package negocio;

import datos.CuentasPenalizadas;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import static negocio.ControlarLANT.BCO;
import static negocio.ControlarLANT.CARTERA;
import static negocio.ControlarLANT.tipoDeCambio;

public class EjecutarQuery {

    public Map<Integer, Double> mapDescuentos = new HashMap<>();

    int primerConsumo = 1;

    List<String> listaPen = new ArrayList<>();

// ============================         QUERY LA COLONIA        =========================================    
    public void queryLaColonia() throws Exception {

        listaPen = CuentasPenalizadas.getCuentasPenalizadas();

        // SELECT * FROM `movimientos` WHERE `producto` LIKE 'VI1303' AND `leyenda` LIKE '%LA COLONIA%'
        //// *************** Nueva Consulta 
        System.out.println("======= Inicia Query La colonia ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE (`grupo_afinidad` = 10 OR `grupo_afinidad` = 11 OR `grupo_afinidad` = 12 ) AND (`leyenda` LIKE 'LA COLONIA%' OR `leyenda` LIKE '______LA COLONIA%' OR `leyenda` LIKE '_____________LA COLONIA%' OR `leyenda` LIKE '______________LA COLONIA%') AND `codop` != 200501 AND producto LIKE '%VI0503%' AND `consumo` LIKE '%CON%'";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.1;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
//                System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //              System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //            System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        //// *************** Segunda Consulta 
        System.out.println("SEGUNDA CONSULTA LA COLONIA");
        stmtInsercion = null;
        laConexion = AdministradorDeConexiones.getConnection();

        laInsercion = "SELECT * FROM `consumos` WHERE (`grupo_afinidad` = 24 "
                + "  OR grupo_afinidad = 130"
                + "    OR grupo_afinidad = 102"
                + "    OR grupo_afinidad = 131"
                + "    OR grupo_afinidad = 127"
                + "    OR grupo_afinidad = 109"
                + "    OR grupo_afinidad = 128"
                + "    OR grupo_afinidad = 38"
                + "    OR grupo_afinidad = 62"
                + "    OR grupo_afinidad = 129) "
                + "AND (`leyenda` LIKE 'LA COLONIA%' "
                + "	OR	`leyenda` LIKE '______LA COLONIA%' "
                + "    OR	`leyenda` LIKE '_____________LA COLONIA%' "
                + "    OR	`leyenda` LIKE '______________LA COLONIA%') "
                + " AND `codop` != 200501"
                + " AND `consumo` LIKE '%CON%'";
        System.out.println("\nConsulta: " + laInsercion);
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            local = rs.getDouble(8);

            //   System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.1;
            if (primerConsumo == 1) {
                //   System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        // ===================================== Descuentos Promo La Colonia
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE `codop` = 950029";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            // System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //  System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // ============================         QUERY LARACH                =========================================    
    public void queryLarach() throws Exception {
        System.out.println("======= Inicia Query Larach ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE `rubro` = 5251 \n"
                + "AND (`grupo_afinidad` = 88 \n"
                + "OR `grupo_afinidad` = 38 \n"
                + "OR `grupo_afinidad` = 62 \n"
                + "OR `grupo_afinidad` = 102 \n"
                + "OR `grupo_afinidad` = 131 \n"
                + "OR `grupo_afinidad` = 113) \n"
                + "AND (`leyenda` LIKE 'LARACH%' \n"
                + "	OR `leyenda` LIKE  '___________LARACH%' \n"
                + "	OR `leyenda` LIKE  '_______LARACH%') \n"
                + "AND `codop` != 200501 \n"
                + "AND `consumo` LIKE '%CON%'";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        
        
        // ===================== MODIFICAR LOS TOPES ACA
        Double topeVI0102 = Redondear.redondear(176.00 * tipoDeCambio);
        Double topeVI0303 = Redondear.redondear(220.00 * tipoDeCambio);
        Double topeVI0404 = Redondear.redondear(264.00 * tipoDeCambio);
        Double topeVI1616 = Redondear.redondear(352.00 * tipoDeCambio);
        Double topeVI1515 = Redondear.redondear(352.00 * tipoDeCambio);
        Double topeVI0503 = Redondear.redondear(350.00 * tipoDeCambio);
        // ===================== MODIFICAR LOS TOPES ACA
        
        
        
        
        
        
        System.out.println("TIPO DE CAMBIO: " + tipoDeCambio);
        System.out.println("TOPE VI0102: " + topeVI0102);
        System.out.println("TOPE VI0303: " + topeVI0303);
        System.out.println("TOPE VI0404: " + topeVI0404);
        System.out.println("TOPE VI1616: " + topeVI1616);
        System.out.println("TOPE VI1515: " + topeVI1515);
        System.out.println("TOPE VI0503: " + topeVI0503);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            //System.out.println("Producto: " + ":" + producto + ":");
            local = local * 0.08;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }

                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        //// *************** Segunda Consulta LARACH
        System.out.println("SEGUNDA CONSULTA LARACH");
        stmtInsercion = null;
        laConexion = AdministradorDeConexiones.getConnection();

        laInsercion = "SELECT * FROM `consumos` WHERE (`grupo_afinidad` = 11 OR `grupo_afinidad` = 13 OR `grupo_afinidad` = 15 ) AND (`leyenda` LIKE 'LARACH%' \n"
                + "	OR `leyenda` LIKE  '___________LARACH%' \n"
                + "	OR `leyenda` LIKE  '_______LARACH%')  AND `codop` != 200501 AND rubro = 5251 AND producto LIKE '%VI0503%' AND `consumo` LIKE '%CON%'";
        System.out.println("\nConsulta: " + laInsercion);
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            local = rs.getDouble(8);

            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.08;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }
                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                if (producto.equals("VI0102")) {
                    if (local > topeVI0102) {
                        local = topeVI0102;
                    }
                }
                if (producto.equals("VI0303")) {
                    if (local > topeVI0303) {
                        local = topeVI0303;
                    }
                }
                if (producto.equals("VI0404")) {
                    if (local > topeVI0404) {
                        local = topeVI0404;
                    }
                }
                if (producto.equals("VI0503")) {
                    if (local > topeVI0503) {
                        local = topeVI0503;
                    }
                }
                if (producto.equals("VI1616")) {
                    if (local > topeVI1616) {
                        local = topeVI1616;
                    }
                }
                if (producto.equals("VI1515")) {
                    if (local > topeVI1515) {
                        local = topeVI1515;
                    }
                }
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo Larach
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE (`codop` = 996008 OR `codop` = 950069 OR `codop` = 950152) ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // ============================         QUERY UNO                   =========================================    
    public void queryUnoPronto() throws Exception {

    }

    // ============================         QUERY KIELSA                =========================================    
    public void queryKielsa() throws Exception {
        System.out.println("======= Inicia Query Kielsa ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE `rubro` = 5912 \n"
                + "                AND `grupo_afinidad` IN (102,72,112,38,62,127) \n"
                + "                 AND (leyenda  LIKE 'KIELSA%' OR leyenda  LIKE '_________KIELSA%')\n"
                + "                AND `codop` != 200501 \n"
                + "                AND `consumo` LIKE '%CON%'";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.1;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo Kielsa
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE (`codop` = 950119 OR `codop` = 996013 ) ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // ============================         QUERY Kielsa Otros         =========================================   
    public void queryKielsaOtros() throws Exception {
        System.out.println("======= Inicia Query Kielsa Otros ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE "
                + " `rubro` != 5912 \n"
                + "                AND `grupo_afinidad` IN (72,112)\n"
                + "                AND (leyenda NOT LIKE 'KIELSA%' OR leyenda NOT LIKE '_________KIELSA%')\n"
                + "                AND (`codop` != 200501 AND `codop` != 100632)\n"
                + "                AND `consumo` LIKE '%CON%';";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);

            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.01;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo La Colonia
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE `codop` = 950129 ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // ============================         QUERY DESCUENTO FICOHSA     =========================================    
    public void queryDescFicohsaUno() throws Exception {
        System.out.println("======= Inicia Query DescFicohsa 1% ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` "
                + "WHERE (`rubro` != 5541 AND `rubro` != 5542) "
                + "AND (`grupo_afinidad` = 64 "
                + "	OR `grupo_afinidad` = 38 "
                + "	OR `grupo_afinidad` = 62 "
                + "	OR `grupo_afinidad` = 110 "
                + "    OR `grupo_afinidad` = 102)  "
                + "AND `producto` NOT LIKE 'VI1616' "
                //+ "	OR `producto` LIKE 'VI0303') "
                + " AND `codop` != 100632 "
                + "AND `consumo` LIKE '%CON%'";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            local = local / 100;
            if (primerConsumo == 1) {
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {

                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo descuentos otros Ficohsa 1%
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE (`codop` = 950139 OR `codop` = 950151) ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            local = local * -1;

            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }
    // ============================         QUERY EDUCACION             =========================================    

    public void queryEducacion() throws Exception {

    }
    // ============================         QUERY SEARS                 =========================================    

    public void querySears() throws Exception {
        System.out.println("======= Inicia Query Sears ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE (`grupo_afinidad` = 62 OR `grupo_afinidad` = 38 OR `grupo_afinidad` = 102) AND `producto` LIKE 'VI1616' \n"
                + "AND `leyenda` LIKE 'SEARS%' \n"
                + "AND (`codop` != 200501 AND `codop` !=  100632) \n"
                + "AND `consumo` LIKE '%CON%' \n"
                + "     \n"
                + "UNION \n"
                + "\n"
                + "SELECT * FROM `consumos` WHERE (`grupo_afinidad` = 2 OR `grupo_afinidad` = 22 OR `grupo_afinidad` = 108) \n"
                + "AND (`producto` LIKE 'VI0102' OR `producto` LIKE 'VI0303' OR `producto` LIKE 'VI0404') \n"
                + "AND `leyenda` LIKE 'SEARS%' \n"
                + "AND (`codop` != 200501 AND `codop` !=  100632) \n"
                + "AND `consumo` LIKE '%CON%' \n"
                + "\n"
                + "UNION \n"
                + "\n"
                + "SELECT * FROM `consumos` WHERE `grupo_afinidad` = 130 \n"
                + "AND `leyenda` LIKE 'SEARS%' AND (`codop` != 200501 AND `codop` !=  100632) \n"
                + "AND `consumo` LIKE '%CON%'		";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.15;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        //// *************** Segunda Consulta SEARS
    /*    System.out.println("SEGUNDA CONSULTA SEARS");
         stmtInsercion = null;
         laConexion = AdministradorDeConexiones.getConnection();

         laInsercion = "SELECT * FROM `consumos` WHERE `grupo_afinidad` = 130 AND `leyenda` LIKE 'SEARS%' AND (`codop` != 200501 AND `codop` !=  100632)  AND `consumo` LIKE '%CON%'";
         System.out.println("\nConsulta: " + laInsercion);
         stmtInsercion = laConexion.createStatement();
         stmtInsercion.execute(laInsercion);
         rs = stmtInsercion.executeQuery(laInsercion);

         while (rs.next()) {
         cuenta = rs.getInt(1);
         gaff = rs.getInt(3);
         codop = rs.getInt(10);
         producto = rs.getString(4);
         leyenda = rs.getString(7);
         local = rs.getDouble(8);

         //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
         local = local * 0.15;
         if (primerConsumo == 1) {
         //    System.out.println("Entro en primer consumo  = 1");
         mapDescuentos.put(cuenta, local);
         primerConsumo = 9;
         } else if (mapDescuentos.containsKey(cuenta)) {
         //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

         local = local + mapDescuentos.get(cuenta);
         //    System.out.println("Local:: " + local);
         mapDescuentos.put(cuenta, local);

         } else {
         //    System.out.println("entre al else: ");
         mapDescuentos.put(cuenta, local);
         }

         i++;
         }
         */
        // ===================================== Descuentos Promo Sears
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE `codop` = 950039  ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }
    // ============================         QUERY INDURA                =========================================   

    public void queryIndura() throws Exception {
        System.out.println("======= Inicia Query Indura ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE  (`grupo_afinidad` = 102 "
                + " OR `grupo_afinidad` = 38 "
                + " OR `grupo_afinidad` = 62)"
                + " AND `leyenda` LIKE '%INDURA%'"
                + " AND `codop` != 200501 "
                + " AND (`rubro` = 6513"
                + "     OR `rubro` = 7011)"
                + " AND `consumo` LIKE '%CON%'";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.15;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo Indura
        System.out.println("================ DESCUENTOS ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE `codop` = 950070 ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // ============================         QUERY DIUNSA                =========================================   
    public void queryDiunsa() throws Exception {
        System.out.println("======= Inicia Query Diunsa ========");

        Statement stmtInsercion = null;
        Connection laConexion = AdministradorDeConexiones.getConnection();

        String laInsercion = "SELECT * FROM `consumos` WHERE `rubro` = 5311 "
                + "AND (grupo_afinidad = 32 "
                + "OR grupo_afinidad = 38 "
                + "OR grupo_afinidad = 40 "
                + "OR grupo_afinidad = 62 "
                + "OR grupo_afinidad = 102 "
                + "OR grupo_afinidad = 140 ) "
                + "AND (`producto` LIKE 'VI0102' OR `producto` LIKE 'MC0102') "
                + "AND `leyenda` LIKE 'DIUNSA%' AND `consumo` LIKE '%CON%' AND (codop != 200501 AND codop != 100632)";
        System.out.println("Consulta: " + laInsercion + "");
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        ResultSet rs = stmtInsercion.executeQuery(laInsercion);
        int i = 1;
        int cuenta, rubro, gaff, codop = 0;
        String producto, tarjeta, leyenda = null, consumo;
        Double local, usd;

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.15;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        //// *************** Segunda Consulta DIUNSA
        System.out.println("SEGUNDA CONSULTA DIUNSA");
        stmtInsercion = null;
        laConexion = AdministradorDeConexiones.getConnection();

        laInsercion = "SELECT * FROM `consumos` WHERE `rubro` = 5311 AND (grupo_afinidad = 32 OR grupo_afinidad = 38 OR grupo_afinidad = 40 OR grupo_afinidad = 62 OR grupo_afinidad = 102 OR grupo_afinidad = 140 OR grupo_afinidad = 129) AND (`producto` LIKE 'VI0303' OR `producto` LIKE 'VI0404' OR `producto` LIKE 'VI1616' OR `producto` LIKE 'VI1303' OR `producto` LIKE 'VI1515' OR `producto` LIKE 'MC0303' OR `producto` LIKE 'MC0404' ) AND `leyenda` LIKE 'DIUNSA%' AND `consumo` LIKE '%CON%' AND (codop != 200501 AND codop != 100632)";
        System.out.println("\nConsulta: " + laInsercion);
        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);
        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            local = rs.getDouble(8);

            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * 0.2;
            if (primerConsumo == 1) {
                //    System.out.println("Entro en primer consumo  = 1");
                mapDescuentos.put(cuenta, local);
                primerConsumo = 9;
            } else if (mapDescuentos.containsKey(cuenta)) {
                //    System.out.println("Local: " + local + " mapdesucnetos.getcuenta: " + mapDescuentos.get(cuenta));

                local = local + mapDescuentos.get(cuenta);
                //    System.out.println("Local:: " + local);
                mapDescuentos.put(cuenta, local);

            } else {
                //    System.out.println("entre al else: ");
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        // ===================================== Descuentos Promo DIUNSA
        System.out.println("================ DESCUENTOS DIUNSA ================ ");
        laInsercion = "SELECT * FROM `codops` WHERE `codop` = 950079 ";

        stmtInsercion = laConexion.createStatement();
        stmtInsercion.execute(laInsercion);

        rs = stmtInsercion.executeQuery(laInsercion);

        while (rs.next()) {
            cuenta = rs.getInt(1);
            gaff = rs.getInt(3);
            codop = rs.getInt(10);
            producto = rs.getString(4);
            leyenda = rs.getString(7);
            consumo = rs.getString(11);
            local = rs.getDouble(8);

            //System.out.println("Cuenta " + cuenta + " CONSUMO:" + consumo);
            //System.out.println("Cuenta: " + cuenta + "  --  Local: " + local);
            local = local * -1;
            //System.out.println("Local en codop: " + local);
            /*if (primerConsumo == 1) {
             mapDescuentos.put(cuenta, local);
             primer
             } else*/
            if (mapDescuentos.containsKey(cuenta)) {
                local = local + mapDescuentos.get(cuenta);
                mapDescuentos.put(cuenta, local);

            } else {
                mapDescuentos.put(cuenta, local);
            }

            i++;
        }

        for (String str : listaPen) {
            mapDescuentos.remove(str);
        }

        System.out.println("=============================================================");
        System.out.println("==========  CODOP " + codop + "  ===================================");
        System.out.println("Sysout de Errores: ");
        for (Map.Entry entry : mapDescuentos.entrySet()) {
            if (0.0 != Double.parseDouble(entry.getValue().toString()) && (0.5 <= Double.parseDouble(entry.getValue().toString()) || -0.5 >= Double.parseDouble(entry.getValue().toString()))) {
                System.out.println(entry.getKey() + ", " + redondear((double) entry.getValue()));
            }

        }
        System.out.println("Tamaño Mapa: " + mapDescuentos.size());
        //System.out.println("Tipo de cambio: " + tipoDeCambio);
        System.out.println("=============================================================");
        System.out.println("=============================================================");

        laConexion.close();

        // }
        stmtInsercion.close();
        this.fileOutPromo(mapDescuentos, leyenda);
        mapDescuentos.clear();
    }

    // Funcion Redondeo
    public static double redondear(double numero) {
        return Math.rint(numero * 100) / 100;

    }

    public void fileOutPromo(Map mapa, String promo) throws IOException {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat dateFormat2 = new SimpleDateFormat("ddMMyyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

        FileWriter fwOb2 = new FileWriter("L:\\Liqui\\" + BCO + "_" + CARTERA + "_" + dateFormat2.format(date) + "_SalidaPromo_" + System.getProperty("user.name") + ".txt", true);
        fwOb2.append(System.getProperty("line.separator") + "================================================================================================================================" + System.getProperty("line.separator") + System.getProperty("line.separator"));
        fwOb2.append(
                "\t\t\t ERRORES EN " + promo + System.getProperty("line.separator") + System.getProperty("line.separator")
                + "\tFecha:   " + dateFormat.format(date) + System.getProperty("line.separator")
                + "\tCartera: " + CARTERA + System.getProperty("line.separator")
                + "\tBanco:   " + BCO + System.getProperty("line.separator") + System.getProperty("line.separator") + System.getProperty("line.separator"));

        int recordsToPrint = mapa.size();

        //FileWriter fstream;
        BufferedWriter out;

        // create your filewriter and bufferedreader
        //fstream = new FileWriter("L:\\Liqui\\730_" + dateFormat2.format(date) + "_SalidaPromo.txt", true);
        out = new BufferedWriter(fwOb2);

        // initialize the record count
        int count = 0;

        // create your iterator for your map
        Iterator<Map.Entry<Integer, Double>> it = mapa.entrySet().iterator();

        // then use the iterator to loop through the map, stopping when we reach the
        // last record in the map or when we have printed enough records
        while (it.hasNext() && count < recordsToPrint) {

            // the key/value pair is stored here in pairs
            Map.Entry<Integer, Double> pairs = it.next();
            //System.out.println("Value is " + pairs.getValue());

            // since you only want the value, we only care about pairs.getValue(), which is written to out
            if (pairs.getValue() > 0.5 || pairs.getValue() < -0.4) {
                out.append("Cuenta: " + String.format("%15s", pairs.getKey())
                        + "\t\t\t"
                        + "Diferencia: " + String.format("%30s", redondear(pairs.getValue()))
                        + "     Error"
                        + System.getProperty("line.separator"));
            }

            // increment the record count once we have printed to the file
            count++;
        }

        //  out.append("================================================================================================================================" + System.getProperty("line.separator") + System.getProperty("line.separator"));
        // lastly, close the file and end
        out.close();

        fwOb2.close();

    }

}
