/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import static java.lang.Double.parseDouble;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author VOIPASER
 */
public class ControlarLANT {

    public static double tipoDeCambio;
    private int moneda = 0;
    private String lineaLeida;
    private String cuenta;
    private String tarjeta;
    private String modLiq;
    private String gAff;
    private String producto;
    private String fechaOrigen;
    private String leyenda;
    private String rubro;
    private String importeMl;
    private String importeUsd;
    private String codop;
    private String grupoMov;
    private int intcuenta;
    public static String BCO;
    public static String CARTERA;
    public static File salidaLant ;
    public static File salidaLant2 ;
    //---------PATRONES--------------------
    Pattern patT014 = Pattern.compile(" --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------T014");
    Pattern patCuenta = Pattern.compile(" CUENTA : (\\d{10})(.*)MOD.LIQ.:\\s+(\\d+)(.*)GRUPO AFINIDAD :\\s+(\\d+)(.*)PROD.: (\\w{6})");//G011
    Pattern patMovimiento = Pattern.compile("^ (\\d{6}) \\d{6} \\d{8}(L|E) (........................................) .*(\\d{4})\\s+(\\d+,\\d{2})\\s+(\\d+,\\d{2})\\s+\\+....... (\\d{6}) \\d{4} \\s+ \\d+,\\d{2} (...) .*I010");//I010

    Pattern patTarjeta = Pattern.compile("^\\s+CAMPOS - T.CARGO : (\\d{19}).*G016");//G016
    Pattern patTCambio = Pattern.compile("(.*)\\s+(\\d+,\\d+)\\+\\s+Q012"); //Q012
    //-------FIN PATRONES ------------------

    String OLD_FORMAT = "ddMMyy";
    String NEW_FORMAT = "yyyy-MM-dd";
    String newDateString = null;

    public void controlarTxt(File file) throws Exception {

        int flag = 0;
        File archivoTxt = file;
        if (!archivoTxt.exists() || !archivoTxt.canRead()) {
            throw new Exception("Archivo inaccesible");
        } else {
            BufferedReader readerMejorado = new BufferedReader(new FileReader(archivoTxt));
            boolean eof = false;
            while (!eof) {
                lineaLeida = readerMejorado.readLine();

                if (lineaLeida != null) {
                    if (lineaLeida.substring(177, 181).equals("A013")) {
                        Pattern patron = Pattern.compile(".*BANCO: (\\d{3})\\s+CARTERA: (\\d{3})");
                        Matcher matchPatron = patron.matcher(lineaLeida);
                        if (matchPatron.find()) {
                        BCO = matchPatron.group(1);
                        CARTERA = matchPatron.group(2);
                        patron = null;
                        matchPatron = null;
                        }
                        
                        
                        salidaLant = new File("L:\\Liqui\\" + BCO + "_" + CARTERA + "_" + System.getProperty("user.name") + "_" + "Lant.csv");
                        salidaLant2 = new File("L:\\Liqui\\" + BCO + "_" + CARTERA + "_" + System.getProperty("user.name") + "_" + "Codop.csv");
                        System.out.println("BCO: " + BCO + "\t\t" + "CARTERA: " + CARTERA);
                        eof = true;
                    }

                }

            }

            FileWriter fwOb2 = new FileWriter(salidaLant, false);
            FileWriter fwOb3 = new FileWriter(salidaLant2, false);

            eof = false;
            while (!eof) {
                lineaLeida = readerMejorado.readLine();

                if (lineaLeida != null) {
                    Matcher matchCuenta = patCuenta.matcher(lineaLeida);

                    if (matchCuenta.find()) {
                        intcuenta++;
                        cuenta = matchCuenta.group(1);

                        gAff = matchCuenta.group(5);
                        producto = matchCuenta.group(7);
                        flag = 1;
                    }

                    /*if (lineaLeida.matches(".*\\d{6}.*\\d{6}.*\\d{6}.* COLONIA.*") && flag == 1) {
                     System.out.println("Cuenta: " + cuenta + "  " + lineaLeida);
                     }*/
                    Matcher matchTarjeta = patTarjeta.matcher(lineaLeida);

                    if (matchTarjeta.find()) {
                        //int fin = matchTarjeta.group(0).length() - 3;
                        tarjeta = matchTarjeta.group(1).substring(0);

                    }

                    Matcher matchTipoCambio = patTCambio.matcher(lineaLeida);
                    if (moneda == 0) {
                        if (matchTipoCambio.find()) {

                            //System.out.println("matcherMoneda.group(2)" + matchTipoCambio.group(2));
                            //System.out.println("matcherMoneda.group(0)" + matchTipoCambio.group(0));
                            tipoDeCambio = parseDouble(matchTipoCambio.group(2).replace(",", "."));
                            moneda = 1;

                        }

                    }

                    Matcher matchMovimiento = patMovimiento.matcher(lineaLeida);
                    if (matchMovimiento.find()) {

                        //System.out.println("Mod liq: " + modLiq + " gAff: " + gAff + " producto: " + producto);
                        // System.out.println("Cuenta: " + cuenta + " Tarjeta: " + tarjeta + "  " + lineaLeida);
                        fechaOrigen = matchMovimiento.group(1);
                        modLiq = matchMovimiento.group(4);
                        leyenda = matchMovimiento.group(3);
                        importeMl = matchMovimiento.group(5);
                        importeUsd = matchMovimiento.group(6);
                        codop = matchMovimiento.group(7);
                        grupoMov = matchMovimiento.group(8);

                        String oldDateString = fechaOrigen;
                        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                        Date d = sdf.parse(oldDateString);
                        sdf.applyPattern(NEW_FORMAT);
                        newDateString = sdf.format(d);

                        // System.out.println("Rubro " + modLiq);
                        //System.out.println("Fecha Origen: " + fechaOrigen
                        //                  +" Leyenda: " + leyenda
                        //                  +" Rubro: " + rubro
                        //                  +" Importe ML: " + importeMl
                        //                  +" Importe Usd: " + importeUsd
                        //                  +" Codop: " + codop
                        //                  +" GrupoMov: " + grupoMov);
                        //System.out.println("");    
                        fwOb2.append(cuenta + ","
                                + modLiq + ","
                                + gAff + ","
                                + producto + ","
                                + tarjeta + ","
                                + newDateString + ","
                                + formatString(leyenda.replaceAll("\'", " ").replaceAll(",", " ").replaceAll("\"", " ")) + ","
                                + importeMl.replace(",", ".") + ","
                                + importeUsd.replace(",", ".") + ","
                                + codop + ","
                                + grupoMov
                                + System.getProperty("line.separator"));

                    }

                    if (lineaLeida.endsWith("BON ( )        I010")) {
                        //System.out.println("Codop: " + lineaLeida);
                        fechaOrigen = lineaLeida.substring(1, 7);
                        DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

                        String oldDateString = fechaOrigen;
                        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                        Date d = sdf.parse(oldDateString);
                        sdf.applyPattern(NEW_FORMAT);
                        newDateString = sdf.format(d);

                        fwOb3.append(cuenta + ","
                                + modLiq + ","
                                + gAff + ","
                                + producto + ","
                                + tarjeta + ","
                                // + lineaLeida.replaceAll("\\s+", " ").replace(",", ".") 
                                + newDateString + ","
                                + formatString(lineaLeida.substring(25, 80).replaceAll("\'", " ").replaceAll(",", " ")) + ","
                                + lineaLeida.substring(92, 107).replaceAll(",", ".").replaceAll("\\s+", "") + ","
                                + lineaLeida.substring(109, 125).replaceAll(",", ".").replaceAll("\\s+", "") + ","
                                + lineaLeida.substring(136, 142).replaceAll(",", ".") + ","
                                + lineaLeida.substring(162, 165).replaceAll(",", ".")
                                + System.getProperty("line.separator"));
                    }

                    Matcher matchT014 = patT014.matcher(lineaLeida);
                    if (matchT014.find()) {
                        flag = 0;
                    }

                } else {
                    eof = true;
                }

            }

            readerMejorado.close();
            fwOb2.close();
            fwOb3.close();
            System.out.println("!!!!!!!!!!!!!!Cuenta: " + intcuenta);

        }

    }

    public static String formatString(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        return temp.replaceAll("[^\\p{ASCII}]", "");
    }

}
