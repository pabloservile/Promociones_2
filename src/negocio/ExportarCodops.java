/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import static negocio.ImportarCodops.CODOPFILE_TXT;
import objetos.Codop;

/**
 *
 * @author VOIPASER
 */
public class ExportarCodops {

    public void exportarCodops(List<Codop> lista) throws IOException {

        FileWriter fwOb = new FileWriter(CODOPFILE_TXT, false);
        PrintWriter pwOb = new PrintWriter(fwOb, false);
        pwOb.flush();
        pwOb.close();
        fwOb.close();
        
        
        FileWriter fwOb2 = new FileWriter(CODOPFILE_TXT, false);
        for (Codop c : lista) {
            fwOb2.append(c.getNroCodop() + ";" + c.getMoneda() + ";" + c.getGrupoMov()
                    + ";" + c.getProcentaje() + ";" + c.getProducto() + ";" + c.getTopeMl()
                    + ";" + c.getTopeUsd() + ";" + c.getFechaDesde() + ";" + c.getFechaHasta()
                    + ";" + c.getDiasAplica() + ";" + c.getLeyenda() + ";" + c.getGrupoAff()+ System.getProperty("line.separator")
            );
            
            
        }
        fwOb2.close();
        //System.out.println("Llegue");
    }

}
