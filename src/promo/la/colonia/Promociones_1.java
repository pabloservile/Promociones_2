/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package promo.la.colonia;

import interfaz.Ventana;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author VOIPASER
 */
public class Promociones_1 {

    /**
     * @param args the command line arguments
     */
      public static void main(String[] args) throws Exception {
        // TODO code application logic here
        ejecutarVentana();
    }

    private static void ejecutarVentana() throws Exception {
        Ventana v = new Ventana();
        v.setSize(700, 700);
        v.pack();

        v.setResizable(true);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dim.getWidth() - v.getWidth()) / 2);
        int y = (int) ((dim.getHeight() - v.getHeight()) / 2);
        v.setLocation(x, y);
        v.setVisible(true);

    }
}
