/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author VOIPASER
 */
public class CuentasPenalizadas {

    public static List<String> getCuentasPenalizadas() throws FileNotFoundException, IOException, Exception {
        System.out.println("LLEGUE");
        DateFormat dateFormat2 = new SimpleDateFormat("yyMMdd");
        Date date = new Date();
        String path = "L:\\Archivos Diarios Control\\";
        
        
        System.out.println("dateFormat: " + dateFormat2.format(date));
    
    
        File[] listaFile = listFilesMatching(new File(path), "730USUDES_" + dateFormat2.format(date) + ".*TXT");

        String nombreUsudes = "" + listaFile[0];

        System.out.println("Nombre Usudes: " + nombreUsudes);

        List<String> listaCuentasPenalizadas = new ArrayList<String>();

        File archivoTxt = new File(nombreUsudes);
        if (!archivoTxt.exists() || !archivoTxt.canRead()) {
            throw new Exception("Archivo inaccesible");
        } else {
            BufferedReader readerMejorado = new BufferedReader(new FileReader(archivoTxt));

            boolean eof = false;
            String lineaLeida;
            String cartera, nombre, cuenta, penalizada;
            int i = 0;
            while (!eof) {
                // Lee una linea entera
                lineaLeida = readerMejorado.readLine();

                if (lineaLeida != null) {

                    cuenta = lineaLeida.substring(0, 10).replaceFirst("^0+(?!$)", "");
                    penalizada = lineaLeida.substring(987, 988);
                    //nombre = lineaLeida.substring(40, 70);
                    cartera = lineaLeida.substring(106, 109);
                    if (penalizada.equals("P")) {
                        if (cartera.equals("010")) {
                            //System.out.println("Cuenta: " + cuenta + " " + penalizada);
                            listaCuentasPenalizadas.add(cuenta);

                            i++;
                        }
                        // System.out.println("Cuenta : " + cuenta + "  --  Penalizada: " + penalizada + "  -- Nombre: " + nombre);
                        // System.out.println("Cartera : " + cartera );

                    }

                } else {
                    eof = true;
                    System.out.println("Total Lista Cuentas Penalizadas: " + i);

                }

            }

        }
        return listaCuentasPenalizadas;
    }

    public static File[] listFilesMatching(File root, String regex) {
        if (!root.isDirectory()) {
            throw new IllegalArgumentException(root + " is no directory.");
        }
        final Pattern p = Pattern.compile(regex); // careful: could also throw an exception!
        return root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return p.matcher(file.getName()).matches();
            }
        });
    }

}
