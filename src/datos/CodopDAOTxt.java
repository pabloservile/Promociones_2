/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static negocio.ImportarCodops.CODOPFILE_TXT;
import static negocio.ImportarCodops.listaCodop;
import objetos.Codop;

/**
 *
 * @author VOIPASER
 */
public class CodopDAOTxt {

    private int fila = 1;
    private String strCodop;
    private String strMoneda;
    private String strGrupoMov;
    private String strPorcentaje;
    private String strProducto;
    private String strTopeMl;
    private String strTopeUsd;
    private String strDesde;
    private String strHasta;
    private String strDiasAplica;
    private String strLeyenda;
    private String strCuotas;

    public List<Codop> importarTxt() {
        listaCodop.clear();
        BufferedReader reader = null;

        Pattern pattern = Pattern.compile("(\\d{6}|.*);(.*);(\\w{3});(.*);(.*);(.*);(.*);(.*);(.*);(.*);(.*);(.*)");

        try {
            File file = new File(CODOPFILE_TXT);
            reader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = reader.readLine()) != null) {

                Matcher matcher = pattern.matcher(line);

                if (matcher.matches()) {
                    strCodop = matcher.group(1);
                    strMoneda = matcher.group(2);
                    strGrupoMov = matcher.group(3);
                    strPorcentaje = matcher.group(4);
                    strProducto = matcher.group(5);
                    strTopeMl = matcher.group(6);
                    strTopeUsd = matcher.group(7);
                    strDesde = matcher.group(8);
                    strHasta = matcher.group(9);
                    strDiasAplica = matcher.group(10);
                    strLeyenda = matcher.group(11);
                    strCuotas = matcher.group(12);

                    Codop c = new Codop(strCodop, strMoneda, strGrupoMov, strPorcentaje,
                            strProducto, Double.parseDouble(strTopeMl), Double.parseDouble(strTopeUsd), strDesde, strHasta, strDiasAplica,
                            strLeyenda, strCuotas);

                    listaCodop.add(c);
                }

                System.out.println(line);
            }

        } catch (IOException e) {
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
        }
        for(Codop c : listaCodop){
            System.out.println("Codop: " + c);
        }
        return listaCodop;
    }
    

}
